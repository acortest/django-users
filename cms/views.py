from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
# Create your views here.

formulario = """
No existe valor en la base de datos para esa llave
<p>Introducela
<p>
<form action="" method="POST">
    valor: <input type="text" name="valor">
    <br/> <input type="submit" value="Enviar">

</form>
"""

@csrf_exempt


def get_content(request, llave):
    #si post
    if request.method == "POST":
        #tomamos los datos del cuerpo
        valor = request.POST['valor']
        #lo metemos en la base de datos
        c = Contenido(clave=llave, valor=valor) #estoy asignando no igualando
        #guardamos la base de datos
        c.save()

    try:
        contenido = Contenido.objects.get(clave=llave) #en la tabla contenidos de todos los objetos dame el que tiene la clave igual a lo que me han pedido
        respuesta = contenido.valor
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            respuesta = formulario
        else:
            respuesta = "Parece que no estas loggeado <a href = /login>Autentifícate aquí </a>"
    return HttpResponse(respuesta)

def index(request):
    #1. Tener la lista de contenidos
    content_list = Contenido.objects.all()
    #2. Cargar la plantilla
    template = loader.get_template('cms/index.html')
    #3. Ligar las variables de la plantilla con las variables Python
    context = {
        #variables_de_la_plantilla : variables_python
        "content_list": content_list
    }
    #4. Renderizar
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Te has logeado con el nombre: " + request.user.username
    else:
        respuesta = "Parece que no estas loggeado <a href = /login>Autentifícate aquí </a>"
    return HttpResponse(respuesta)


def loggedOut(request):
    logout(request)
    return redirect("/cms/")







